import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-business-card',
    template: `        
            <div (click)="goToBusinessPage(business.id)" class="card membuzz-card">
                <div class="card-header membuzz-card__header">
                    <img [src]="getIcon(business)" width="50" height="50">
                </div>
                <div class="card-block">
                    <h4 class="card-title">{{business.name}}</h4>
                </div>
                <div *ngIf="!showConfirmButtons" class="card-footer text-right">
                    <button (click)="editBusiness(business.id); $event.stopPropagation();" type="button" class="btn btn-lg btn-outline-primary">
                        <i class="icon-pencil"></i>
                    </button>
                    <button (click)="deleteBusiness(business.id); $event.stopPropagation();" type="button" class="btn btn-lg btn-outline-primary">
                        <i class="icon-trash"></i>
                    </button>
                </div>
                <div *ngIf="showConfirmButtons" class="card-footer text-right">
                    <button (click)="deleteBusinessConfirm(true); $event.stopPropagation();" type="button" class="btn btn-lg btn-outline-danger">
                        <i class="icon-check"></i>
                    </button>
                    <button (click)="deleteBusinessConfirm(false); $event.stopPropagation();" type="button" class="btn btn-lg btn-outline-primary">
                        <i class="icon-close"></i>
                    </button>
                </div>
            </div>`,
    styles: [
        `.membuzz-card {
            cursor: pointer;
            user-select: none;
        }`,
        `.membuzz-card__header {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }`
    ]
})
export class BusinessCardComponent implements OnInit {
    @Input('business') business;
    @Output('removeBusiness') removeBusiness = new EventEmitter<any>();

    public showConfirmButtons: boolean;

    constructor(
        private router: Router
    ) {}

    ngOnInit() {
        // console.log('this.business', this.business);
    }

    public goToBusinessPage(id: string): void {
        this.router.navigate(['business', id]);
    }

    public editBusiness(id: string): void {
        this.router.navigate(['business', id], { queryParams: { edit: true } });
    }

    public deleteBusiness(id: string): void {
        // console.log('delete this business', id);
        this.showConfirmButtons = true;
    }

    public deleteBusinessConfirm(confirm): void {
        if (confirm) {
            // console.log('Delete', this.business.name);
            this.removeBusiness.emit();
        }

        this.showConfirmButtons = false;
    }

    public getIcon(business) {
        if (business.image && business.image !== 'none') return business.image;

        return business.icon;
    }
}