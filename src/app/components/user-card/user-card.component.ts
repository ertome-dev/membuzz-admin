import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-user-card',
    template: `        
            <div (click)="goToUserPage(user.id)" class="card membuzz-card">
                <div class="card-header membuzz-card__header">
                    <img src="{{userImage}}" width="50" height="50">
                    <strong>{{user.us_email}}</strong>
                </div>
                <div class="card-block">
                    <h4 class="card-title">{{user.us_name}}</h4>
                </div>
                <div *ngIf="!showConfirmButtons" class="card-footer text-right">
                    <button (click)="editUser(user.id); $event.stopPropagation();" type="button" class="btn btn-lg btn-outline-primary">
                        <i class="icon-pencil"></i>
                    </button>
                    <button (click)="deleteUser(user.id); $event.stopPropagation();" type="button" class="btn btn-lg btn-outline-primary">
                        <i class="icon-trash"></i>
                    </button>
                </div>
                <div *ngIf="showConfirmButtons" class="card-footer text-right">
                    <button (click)="deleteUserConfirm(true); $event.stopPropagation();" type="button" class="btn btn-lg btn-outline-danger">
                        <i class="icon-check"></i>
                    </button>
                    <button (click)="deleteUserConfirm(false); $event.stopPropagation();" type="button" class="btn btn-lg btn-outline-primary">
                        <i class="icon-close"></i>
                    </button>
                </div>
            </div>`,
    styles: [
        `.membuzz-card {
            cursor: pointer;
            user-select: none;
        }`,
        `.membuzz-card__header {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }`
    ]
})
export class UserCardComponent implements OnInit {
    @Input('user') user;
    @Output('removeUser') removeUser = new EventEmitter<any>();

    public showConfirmButtons: boolean;
    public userImage: string;

    constructor(
        private router: Router
    ) {}

    ngOnInit() {
        if (this.user.us_image) {
            const userImageArray = this.user.us_image.split('/');
            this.userImage = userImageArray[userImageArray.length - 1] === 'none' ? 'assets/img/avatar.png' : this.user.us_image;
        } else {
            this.userImage = 'assets/img/avatar.png';
        }
    }

    public goToUserPage(id: string): void {
        this.router.navigate(['users', id]);
    }

    public editUser(id: string): void {
        this.router.navigate(['users', id], { queryParams: { edit: true } });
    }

    public deleteUser(id: string): void {
        console.log('delete this user', id);
        this.showConfirmButtons = true;
    }

    public deleteUserConfirm(confirm): void {
        if (confirm) {
            console.log('Delete', this.user.us_name);
            this.removeUser.emit();
        }

        this.showConfirmButtons = false;
    }
}