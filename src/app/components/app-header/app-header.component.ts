import { Component } from '@angular/core';
import { Router } from '@angular/router';

// Services
import { LoginService } from '../../services';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html'
})
export class AppHeaderComponent {

    constructor(
        private router: Router,
        private loginService: LoginService
    ) {

    }

    public goToSettings() {
      console.log('settings');
    }

    public logout() {
        this.loginService.logout();
        this.router.navigateByUrl('/login');
    }

    public goToLocation() {
        console.log('go to location');
    }
}
