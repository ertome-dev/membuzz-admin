import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-deal-card',
    template: `        
            <div (click)="goToDealPage(deal.id)" class="card membuzz-card">
                <div class="card-header membuzz-card__header">
                    <strong>{{deal.name}}</strong>
                </div>
                <div class="card-block">
                    <div>
                        <strong class="card-title">Full Price</strong>
                        <span class="card-text">{{deal.full_price}}</span>
                    </div>
                    <div>
                        <strong class="card-title">Deal Price</strong>
                        <span class="card-text">{{deal.deal_price}}</span>
                    </div>
                    <div>
                        <strong class="card-title">Discount</strong>
                        <span class="card-text">{{(deal.discount * 100) + '%'}}</span>
                    </div>
                </div>
                <div *ngIf="!showConfirmButtons" class="card-footer text-right">
                    <button (click)="editDeal(deal.id); $event.stopPropagation();" type="button" class="btn btn-lg btn-outline-primary">
                        <i class="icon-pencil"></i>
                    </button>
                    <button (click)="deleteDeal(deal.id); $event.stopPropagation();" type="button" class="btn btn-lg btn-outline-primary">
                        <i class="icon-trash"></i>
                    </button>
                </div>
                <div *ngIf="showConfirmButtons" class="card-footer text-right">
                    <button (click)="deleteDealConfirm(true); $event.stopPropagation();" type="button" class="btn btn-lg btn-outline-danger">
                        <i class="icon-check"></i>
                    </button>
                    <button (click)="deleteDealConfirm(false); $event.stopPropagation();" type="button" class="btn btn-lg btn-outline-primary">
                        <i class="icon-close"></i>
                    </button>
                </div>
            </div>`,
    styles: [
        `.membuzz-card {
            cursor: pointer;
            deal-select: none;
        }`,
        `.membuzz-card__header {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }`
    ]
})
export class DealCardComponent implements OnInit {
    @Input('deal') deal;
    @Output('removeDeal') removeDeal = new EventEmitter<any>();

    public showConfirmButtons: boolean;

    constructor(
        private router: Router
    ) {}

    ngOnInit() {
        console.log('this.deal', this.deal);
    }

    public goToDealPage(id: string): void {
        this.router.navigate(['deals', id]);
    }

    public editDeal(id: string): void {
        this.router.navigate(['deals', id], { queryParams: { edit: true } });
    }

    public deleteDeal(id: string): void {
        console.log('delete this deal', id);
        this.showConfirmButtons = true;
    }

    public deleteDealConfirm(confirm): void {
        if (confirm) {
            this.removeDeal.emit();
        }

        this.showConfirmButtons = false;
    }
}