import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/Rx'; // for .map using

@Injectable()
export class CouponService {
    private _host: string;
    private _headers: Headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

    constructor(
        private http: Http
    ) {
        this._host = environment.host;
    }

    public addCoupon(coupon) { // {"caf_id":"6621", "buisness_name":"qwerty", "c_photo":"", "discount":"10", "bs_wrk_st":"10:00:00", "bs_wrk_end":"14:00:00", "full_price":"1000", "deal_price":"899", "deal_st":"2017-01-15", "deal_end":"2017-01-25", "avalb_items":"10", "purchases":"5", "deal_content":"kqh eoiwhr wehr klahdf", "qnt":"10"}
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'add_coupon', 'token': _token, 'coupon': coupon});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });

    }

    public updateCoupon(coupon) {  // {"id":"2", "caf_id":"6620", "buisness_name":"song", "c_photo":"", "discount":"10", "bs_wrk_st":"10:00:00", "bs_wrk_end":"14:00:00", "full_price":"1000", "deal_price":"899", "deal_st":"2017-01-15", "deal_end":"2017-01-25", "avalb_items":"10", "purchases":"5", "deal_content":"kqh eoiwhr wehr klahdf", "qnt":"10"}
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'update_coupon', 'token': _token, 'coupon': coupon});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    public getCouponList() { // {"tag":"get_coupon_list", "token":"1c87d29977a1892b0c8ea9b23859f302"} - get list of coupons
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'get_coupon_list', 'token': _token});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    public getCouponById(id) {
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'get_coupon_by_id', 'token': _token, 'id': id});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    public deleteCoupon(id) { // {"tag":"delete_coupon", "token":"1c87d29977a1892b0c8ea9b23859f302", "id":"1"} - delete coupon
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'delete_coupon', 'token': _token, 'id': id});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    public buyCoupons(cafId, quantity) { // {"tag":"buy_coupons", "token":"55e59d0b10d3c20b8ea8dd066151c5c3", "caf_id":"6671", "col":"2"} - buy coupons
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'buy_coupons', 'token': _token, 'caf_id': cafId, 'col': quantity});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }


    /////
    private _setRequest(data: string): Promise<any> {
        const body: URLSearchParams = new URLSearchParams();
        body.set('data', data);
        return new Promise((resolve, reject) => {
            this.http.post(this._host, body.toString(), { headers: this._headers })
                .map(response => response.json())
                .subscribe((response) => {
                    if (response.success) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                }, (error) => {
                    reject(error);
                });
        });
    }


}