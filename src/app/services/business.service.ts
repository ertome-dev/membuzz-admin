import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/Rx'; // for .map using

@Injectable()
export class BusinessService {
    private _host: string;
    private _headers: Headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

    constructor(
        private http: Http
    ) {
        this._host = environment.host;
    }

    getBusinessList() {
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'get_user_caf_list', 'token': _token});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    getBusinessAround() {
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'get_caf_around', 'token': _token, 'lat': '46.474599', 'lon': '30.755343'});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    getBusinessById(id) {
        console.warn('id ==> ', id);
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'get_user_caf', 'token': _token, 'caf_id': id});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    createBusiness(business) { // "cafe":{"id":"sdjghsdihg", "icon":"sjdhfksdhg", "name":"qwerty", "place_id":"fdhgjgkjfyuhk56u76jj6", "rating":"4.9", "vicinity":"dsgdfgdfghdfg", "lat":"32.32543", "lon":"45.32452"}
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'add_cafe', 'token': _token, 'cafe': business});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    deleteBusiness(id) {
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'delete_cafe', 'token': _token, 'id': id});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    editBusiness(cafe) {
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'update_cafe', 'token': _token, 'cafe': cafe});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    /////
    private _setRequest(data: string): Promise<any> {
        const body: URLSearchParams = new URLSearchParams();
        body.set('data', data);
        return new Promise((resolve, reject) => {
            this.http.post(this._host, body.toString(), { headers: this._headers })
                .map(response => response.json())
                .subscribe((response) => {
                    // if (response.success) {
                        resolve(response);
                    // } else {
                    //     reject(response);
                    // }
                }, (error) => {
                    reject(error);
                });
        });
    }

}