import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/Rx'; // for .map using

@Injectable()
export class LoginService {
    private _host: string;
    private _headers: Headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

    constructor(
        private http: Http
    ) {
        this._host = environment.host;
    }

    isLoggedIn(): boolean {
        const token = localStorage.getItem('membuzz_admin_token');
        return !!token;
    }

    login(user) {
        const data: string = JSON.stringify({tag: 'login_user', email: user.email, pass: user.password});
        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                localStorage.setItem('membuzz_admin_token', response.user._token);
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    logout() {
        localStorage.removeItem('membuzz_admin_token');
    }

    /////
    private _setRequest(data: string): Promise<any> {
        const body: URLSearchParams = new URLSearchParams();
        body.set('data', data);
        return new Promise((resolve, reject) => {
            this.http.post(this._host, body.toString(), { headers: this._headers })
                .map(response => response.json())
                .subscribe((response) => {
                    if (response.success) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                }, (error) => {
                    reject(error);
                });
        });
    }

}