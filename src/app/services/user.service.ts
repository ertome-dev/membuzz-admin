import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/Rx'; // for .map using

@Injectable()
export class UserService {
    private _host: string;
    private _headers: Headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

    constructor(
        private http: Http
    ) {
        this._host = environment.host;
    }

    public createUser(user: any): Promise<any> { // {"us_email":"neww@qq.qq", "us_pass":"111111", "us_name":"alfred", "us_image":"jseof.jpg", "status":"1", "type":"3"}
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'create_user', 'token': _token, 'user': user});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    public updateUser(user: any): Promise<any> { // {"id":"3", "us_email":"qq@qq.qq", "us_pass":"111111", "us_name":"bruce wayne", "status":"1", "type":"2", "birth_date":"1980-01-01"}
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'update_user', 'token': _token, user: user});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }
    //
    public getUser(id: string): Promise<any> {
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'get_user_by_id', 'token': _token, 'user_id': id});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    public getBusinessUsers(businessId: any): Promise<any> {
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({'tag': 'get_caf_users_list', 'token':_token, 'caf_id': businessId});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    public getUserList(): Promise<any> { //{"tag":"get_users_list", "token":"1c87d29977a1892b0c8ea9b23859f302"}
        const _token: string = localStorage.getItem('membuzz_admin_token');
        const data: string = JSON.stringify({tag: 'get_users_list', 'token': _token});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    public getUserTypes(): Promise<any> {
        const data: string = JSON.stringify({tag: 'get_user_types'});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    public getUserStatuses(): Promise<any> {
        const data: string = JSON.stringify({tag: 'get_user_statuses'});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }

    public getUserGenders(): Promise<any> {
        const data: string = JSON.stringify({tag: 'get_user_genders'});

        return new Promise((resolve, reject) => {
            this._setRequest(data).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }


    /////
    private _setRequest(data: string): Promise<any> {
        const body: URLSearchParams = new URLSearchParams();
        body.set('data', data);
        return new Promise((resolve, reject) => {
            this.http.post(this._host, body.toString(), { headers: this._headers })
                .map(response => response.json())
                .subscribe((response) => {
                    if (response.success) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                }, (error) => {
                    reject(error);
                });
        });
    }
}