export const navigation = [
    {
        name: 'Users Manager',
        url: '/users',
        icon: 'icon-people',
        children: [
            {
                name: 'New User',
                url: '/users/create',
                icon: 'icon-plus',
            },
            {
                name: 'Users List',
                url: '/users',
                icon: 'icon-list',
            }
        ]
    },
    {
        name: 'Business Manager',
        url: '/business',
        icon: 'icon-graph',
        children: [
            {
                name: 'New Business',
                url: '/business/create',
                icon: 'icon-plus',
            },
            {
                name: 'Business List',
                url: '/business',
                icon: 'icon-list',
            }
        ]
    },
    {
        name: 'Deal Manager',
        url: '/deals',
        icon: 'icon-basket',
        children: [
            {
                name: 'New Coupon',
                url: '/deals/create',
                icon: 'icon-plus',
            },
            {
                name: 'Coupon List',
                url: '/deals',
                icon: 'icon-list',
            }
        ]
    },
    {
        name: 'Points Manager',
        url: '/point-management',
        icon: 'icon-chart',
    },
    {
        name: 'Smart Agent',
        url: '/smart-agent',
        icon: 'icon-star'
    },
    {
        name: 'Use of Coupon',
        url: '/use-coupon',
        icon: 'icon-credit-card'
    },
    {
        name: 'Contact us Manager',
        url: '/contact-us',
        icon: 'icon-question'
    },
];