import { NgModule } from '@angular/core';
import {
    Routes,
    RouterModule
} from '@angular/router';

import { ContactUsComponent } from './contact-us.component';


const routes: Routes = [
    {
        path: '',
        component: ContactUsComponent,
        data: {
            title: 'Contact Us'
        }
        // children: [
        //     {
        //         path: '',
        //         component: BusinessListComponent,
        //         data: {
        //             title: 'Business'
        //         }
        //     },
        //     {
        //         path: ':id',
        //         component: BusinessProfileComponent,
        //         data: {
        //             title: 'Business'
        //         }
        //     },
        //     {
        //         path: ':id/:edit',
        //         component: BusinessProfileComponent,
        //         data: {
        //             title: 'Edit Business'
        //         }
        //     },
        //     {
        //         path: '/create',
        //         component: BusinessProfileComponent,
        //         data: {
        //             title: 'New Business'
        //         }
        //     }
        // ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ContactUsRoutingModule {}