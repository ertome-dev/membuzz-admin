import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ContactUsComponent } from './contact-us.component';

import { ContactUsRoutingModule } from './contact-us-routing.module';



@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        RouterModule,
        ContactUsRoutingModule,
    ],
    declarations: [
        ContactUsComponent,
    ]
})
export class ContactUsModule { }
