import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { LoginComponent } from './login.component';
import { RegisterComponent } from './register.component';
import { ForgotPasswordComponent } from './forgot-password.component';
import { P404Component } from './404.component';
import { P500Component } from './500.component';

import { PagesRoutingModule } from './pages-routing.module';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        PagesRoutingModule
    ],
    declarations: [
        LoginComponent,
        RegisterComponent,
        ForgotPasswordComponent,
        P404Component,
        P500Component
    ]
})
export class PagesModule { }
