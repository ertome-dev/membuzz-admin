import { Component } from '@angular/core';
import { Router } from '@angular/router';


@Component({
    templateUrl: 'forgot-password.component.html'
})
export class ForgotPasswordComponent {
    public emailSended = false;
    public user = {
        email: ''
    };

    constructor(
        private router: Router,
    ) { }

    restorePassword(email) {
        console.log('email ==> ', email);
        this.emailSended = true;
    }

    goTo(page) {
        switch (page) {
            case 'login':
                this.router.navigateByUrl('/login');
                break;

            default:
                console.log('invalid page name', page);
                break;
        }
    }


}
