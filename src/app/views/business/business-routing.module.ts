import { NgModule } from '@angular/core';
import {
    Routes,
    RouterModule
} from '@angular/router';

import { BusinessComponent } from './business.component';
import { BusinessListComponent } from './list.component';
import { BusinessProfileComponent } from './business-profile.component';
// import { CreateBusinessComponent } from './create.component';

const routes: Routes = [
    {
        path: '',
        component: BusinessComponent,
        children: [
            {
              path: '',
                component: BusinessListComponent,
                data: {
                  title: 'Business'
                }
            },
            {
                path: ':id',
                component: BusinessProfileComponent,
                data: {
                    title: 'Business'
                }
            },
            {
                path: ':id/:edit',
                component: BusinessProfileComponent,
                data: {
                    title: 'Edit Business'
                }
            },
            {
                path: '/create',
                component: BusinessProfileComponent,
                data: {
                    title: 'New Business'
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BusinessRoutingModule {}
