import { Component, OnInit } from '@angular/core';
import { EmailValidator } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { BusinessService, UserService } from '../../services';

import * as moment from 'moment';


@Component({
    templateUrl: 'business-profile.component.html'
})
export class BusinessProfileComponent implements OnInit {
    public months = [];
    public years = [];
    public days = {
        lastDeal: [],
        lastDealExpiry: [],
        lastDealRequest: []
    };
    public business: any = null;

    public collapsed: any = {
        users: true,
        deals: true,
        coupons: true
    };

    users: any[] = [];


    public tmp_deals = [
        {
            name: 'test deal 1'
        },
        {
            name: 'test deal 2'
        },
        {
            name: 'test deal 3'
        },
        {
            name: 'test deal 4'
        }
    ];

    public tmp_last_deal_date = {
        day: Number(moment().format('DD')),
        year:  Number(moment().format('YYYY')),
        month:  Number(moment().format('MM')),
        time:  moment().format('HH:mm')
    };

    public tmp_last_deal_expiry_date = {
        day: Number(moment().format('DD')),
        year:  Number(moment().format('YYYY')),
        month:  Number(moment().format('MM')),
        time:  moment().format('HH:mm')
    };

    public tmp_last_date_deal_request = {
        day: Number(moment().format('DD')),
        year:  Number(moment().format('YYYY')),
        month:  Number(moment().format('MM')),
        time:  moment().format('HH:mm')
    };



    public daysMonths = [];
    public editable: boolean = false;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private businessService: BusinessService,
        private userService: UserService
    ) {
        console.log('moment', moment());
        console.log('this.tmp_last_deal_date', this.tmp_last_deal_date)
    }

    ngOnInit() {
        this._getBusiness();
        this._isEditable();
        // this._yearGenerate();
        // this._monthsGenerate();
        // this._daysGenerate();
        this._yearGenerate();
        this._monthsGenerate();

        this.daysGenerate(this.tmp_last_deal_date.month, this.tmp_last_deal_date.year, 'lastDeal');
        this.daysGenerate(this.tmp_last_deal_expiry_date.month, this.tmp_last_deal_expiry_date.year, 'lastDealExpiry');
        this.daysGenerate(this.tmp_last_date_deal_request.month, this.tmp_last_date_deal_request.year, 'lastDealRequest');

    }

    public cancel() {
        this.router.navigate(['users']);
    }

    public submit() {
        // const birth = moment(`${this.user.birthYear} ${this.user.birthMonth} ${this.user.birthDay}`).format('YYYY-MM-DD');

        console.log('business', this.business);
    }

    public daysGenerate(month, year, type) {
        this.days[type] = [];
        for (let j = 1, daysLength = moment(`${year}-${month}`, 'YYYY-MM').daysInMonth(); j <= daysLength; j++) {
            this.days[type].push(j);
        };
    }

    public editBusiness() {
         // "cafe":{"id":"sdjghsdihg", "icon":"qasw", "name":"qwerty123", "place_id":"fdhgjgkjfyuhk56u76jj6", "rating":"4.9", "vicinity":"dsgdfgdfghdfg", "lat":"32.32543", "lon":"45.32452"}
        this.businessService.editBusiness(this.business).then(response => {
            console.log('response', response);
        });
    }

    ////

    private _getBusiness() {
        this.activatedRoute.params.subscribe(params => {
            if (params.id === 'create') {
                this.editable = true;
                this.business = {};
                // this.user.birth_date = moment().add('year', -18).format('YYYY-MM-DD');
                // this._userDateParse(this.user.birth_date, this.daysGenerate.bind(this));
            } else if (params.id) {
                // get business by id
                this.businessService.getBusinessById(params.id).then((response: any) => {
                    console.log('business response', response);
                    this.business = response.cafe;

                    this._mapInit(this.business.lat, this.business.lon, this.editable);

                    this.userService.getBusinessUsers(response.cafe.id).then((usersResponse: any) => {
                        this.users = usersResponse.users;
                        console.log('this.users', this.users)
                    });
                });
                // this._userDateParse(this.user.birth_date, this.daysGenerate.bind(this));
            }
        });
        // async get user

    }

    private _isEditable() {
        this.activatedRoute.queryParams.subscribe(params => {
            if (params && params.hasOwnProperty('edit') && params.edit) {
                this.editable = true;
            }
        });

    }

    private _monthsGenerate() {
        this.months = [];
        for (let i = 1, monthsLength = 12; i <= monthsLength; i++) {
            this.months.push(i);
        };
    }

    private _yearGenerate() {
        this.years = [];
        const currentYear = Number(moment().format('YYYY'));
        for (let i = 0, length = 100; i < length; i++ ) {
            this.years.push(currentYear - i);
        }
    }

    private _mapInit(lat, lon, editable) {
        setTimeout((() => {
            const google = (window as any).google;
            const center = new google.maps.LatLng(Number(lat), Number(lon));
            const element = document.getElementById('map');
            const map = new google.maps.Map(element, {
                zoom: 14,
                center: center
            });
            let marker = new google.maps.Marker({
                position: center,
                map: map
            });
            console.log('marker', marker);
            if (editable) {
                google.maps.event.addListener(map, 'click', (e) => {
                    marker.setPosition(e.latLng);
                    this.business.lat = e.latLng.lat();
                    this.business.lon = e.latLng.lng();
                });
            }
        }).bind(this));
    }
}
