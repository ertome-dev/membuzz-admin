import { Component, OnInit } from '@angular/core';
import { BusinessService } from '../../services';
import * as moment from 'moment';

@Component({
    template: `
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Filter by</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                id="name"
                                                placeholder="Business name"
                                                [(ngModel)]="business.name"
                                        >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Gender</label>
                                        <select class="form-control" id="type" [(ngModel)]="business.gender">
                                            <option *ngIf="business.gender !== ''" value="{{''}}">
                                                Stop filter by gender
                                            </option>
                                            <option value="1">gender 1</option>
                                            <option value="2">gender 2</option>
                                            <option value="3">gender 3</option>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="age_from">Age</label>
                                        <select class="form-control" id="age_from" [(ngModel)]="business.age_from">
                                            <option
                                                    *ngIf="business.ageLimit !== ''"
                                                    value="{{''}}"
                                            >Stop filter by age</option>
                                            <option
                                                    value="0"
                                            >0+</option>
                                            <option
                                                    value="6"
                                            >6+</option>
                                            <option
                                                    value="16"
                                            >16+</option>
                                            <option
                                                    value="18"
                                            >18+</option>
                                            <option
                                                    value="21"
                                            >21+</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col--><!--/.col-->
        
            <div class="row">
                <div *ngFor="let business of filterBusinessList(businessList); let index = index;" class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                    <app-business-card [business]="business" (removeBusiness)="this.removeBusiness(index)"></app-business-card>
                </div>
            </div>
            
          
                <p class="text-center text-justify">
                    <span *ngIf="businessList.length === 0">
                        No business.
                    </span>
                    <span *ngIf="businessList.length !== 0 && filterBusinessList(businessList).length === 0">
                        No business with these filter params.
                    </span>
                </p>
            
        </div>
    `,
})

export class BusinessListComponent implements OnInit {
    public businessList: any = [];
    public business = {
        name: '',
        gender: '',
        age_from: '',
    };

    public userList = [
        {"id":"1", "us_email":"q1@qq.qq", "us_pass":"111111", "us_name":"batman", "status":"1", "type":"2", "birth_date":"2014-01-01"},
        {"id":"2", "us_email":"q2@qq.qq", "us_pass":"111111", "us_name":"superman", "status":"1", "type":"1", "birth_date":"2018-01-01"},
        {"id":"3", "us_email":"qsdfsdfsdfsdfsd3@qq.qq", "us_pass":"111111", "us_name":"spiderman", "status":"1", "type":"2", "birth_date":"1991-01-01"},
        {"id":"4", "us_email":"q4@qq.qq", "us_pass":"111111", "us_name":"iron man", "status":"2", "type":"1", "birth_date":"2000-01-01"},
    ];

    constructor(
        private businessService: BusinessService
    ) {}

    ngOnInit() {
        this._getBusinessList();
    }

    public filterBusinessList(businessList) {
        return businessList.filter(business => {
            const matchByFilterResult =
                (this.business.name ? (business.name.toLowerCase().indexOf(this.business.name.toLowerCase()) === 0) : true) &&
                (this.business.gender ? ('' + business.gender === '' + this.business.gender) : true) &&
                (this.business.age_from ? (Number(this.business.age_from) >= business.age_from) : true);

            return matchByFilterResult;
        });
    }

    removeBusiness(index) {
        this.businessList.splice(index, 1);
    }

    ///
    _getBusinessList() {
        // this.businessService.getBusinessList().then((response) => {
        //     // zone.js:2935 POST http://188.226.145.163/membuzz_back/public/datacenter 500 (Internal Server Error)
        //     console.log('response', response);
        // });
        this.businessService.getBusinessAround().then((response: any) => {
            this.businessList = response.cafs;
            // console.log('this.businessList', this.businessList);
        });
    }

}