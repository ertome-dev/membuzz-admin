import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CollapseModule } from 'ngx-bootstrap/collapse';

import { BusinessComponent } from './business.component';
import { BusinessProfileComponent } from './business-profile.component';
import { BusinessListComponent } from './list.component';

import { BusinessCardComponent } from '../../components/business-card/business-card.component';

import { BusinessRoutingModule } from './business-routing.module';



@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    RouterModule,
    BusinessRoutingModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    CollapseModule.forRoot(),
  ],
  declarations: [
      BusinessComponent,
      BusinessProfileComponent,
      BusinessListComponent,
      BusinessCardComponent
  ]
})
export class BusinessModule { }
