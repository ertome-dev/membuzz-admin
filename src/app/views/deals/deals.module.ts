import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { DealsComponent } from './deals.component';
import { DealProfileComponent } from './deal-profile.component';
import { DealsListComponent } from './list.component';

import { DealCardComponent } from '../../components/deal-card/deal-card.component';

import { DealsRoutingModule } from './deals-routing.module';



@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    RouterModule,
    DealsRoutingModule,
    BsDropdownModule,
    ButtonsModule.forRoot()
  ],
  declarations: [
      DealsComponent,
      DealProfileComponent,
      DealsListComponent,
      DealCardComponent
  ]
})
export class DealsModule { }
