import { NgModule } from '@angular/core';
import {
    Routes,
    RouterModule
} from '@angular/router';

import { DealsComponent } from './deals.component';
import { DealsListComponent } from './list.component';
import { DealProfileComponent } from './deal-profile.component';
// import { CreateDealComponent } from './create.component';

const routes: Routes = [
    {
        path: '',
        component: DealsComponent,
        children: [
            {
              path: '',
                component: DealsListComponent,
                data: {
                  title: 'Deals'
                }
            },
            {
                path: ':id',
                component: DealProfileComponent,
                data: {
                    title: 'Deal'
                }
            },
            {
                path: ':id/:edit',
                component: DealProfileComponent,
                data: {
                    title: 'Edit Deal'
                }
            },
            {
                path: '/create',
                component: DealProfileComponent,
                data: {
                    title: 'New Deal'
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DealsRoutingModule {}
