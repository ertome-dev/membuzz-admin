import { Component, OnInit } from '@angular/core';
import { EmailValidator } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

@Component({
    templateUrl: 'deal-profile.component.html'
})
export class DealProfileComponent implements OnInit {
    public months = [];
    public years = [];
    public days: any = {
        startDate: [],
        endDate: []
    };
    public deal: any = null;


    public editable: boolean = false;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
    ) {
        console.log('moment', moment());
    }

    ngOnInit() {
        this._getDeal();
        this._isEditable();
        this._yearGenerate();
        this._monthsGenerate();
        // this._daysGenerate();

    }

    public cancel() {
        this.router.navigate(['deals']);
    }

    public submit() {
        this.router.navigate(['deals']);
    }

    public daysGenerate(month, year, type) {
        this.days[type] = [];
        for (let j = 1, daysLength = moment(`${year}-${month}`, 'YYYY-MM').daysInMonth(); j <= daysLength; j++) {
            this.days[type].push(j);
        };
    }

    ////

    private _getDeal() {
        this.activatedRoute.params.subscribe(params => {
            if (params.id === 'create') {
                this.editable = true;
                this.deal = {};

                this.deal.deal_start_date = this._dealDateParse(this.deal.deal_start_date);
                this.deal.deal_end_date = this._dealDateParse(this.deal.deal_end_date);

                this.daysGenerate(this.deal.deal_start_date.m, this.deal.deal_start_date.y, 'startDate');
                this.daysGenerate(this.deal.deal_start_date.m, this.deal.deal_start_date.y, 'endDate');
            } else {
                // get deal by id
                this.deal = {
                    'id': 1,
                    'name': 'deal example',
                    'full_price': 100,
                    'deal_price': 75,
                    'discount': .25,
                    'deal_redemption': 1, // Once a day per user / One time only per user
                    'deal_content': 'deal content',
                    'deal_photo': 'photo path',
                    'deal_text': 'deal text',
                    'deal_items': 1,
                    'deal_purchase': 2,
                    'deal_start_date': '02-03-2018',
                    'deal_end_date': '03-03-2018'
                };
                this.deal.deal_start_date = this._dealDateParse(this.deal.deal_start_date);
                this.deal.deal_end_date = this._dealDateParse(this.deal.deal_end_date);

                this.daysGenerate(this.deal.deal_start_date.m, this.deal.deal_start_date.y, 'startDate');
                this.daysGenerate(this.deal.deal_start_date.m, this.deal.deal_start_date.y, 'endDate');
            }
        });
        // async get deal

    }

    private _dealDateParse(usrDate) {
        const dealBirthDay = moment(usrDate);

        return {
            d: dealBirthDay.format('D'),
            m: dealBirthDay.format('M'),
            y: dealBirthDay.format('YYYY')
        };

    }

    private _monthsGenerate() {
        this.months = [];
        for (let i = 1, monthsLength = 12; i <= monthsLength; i++) {
            this.months.push(i);
        };
    }

    private _yearGenerate() {
        this.years = [];
        const currentYear = Number(moment().format('YYYY'));
        for (let i = 0, length = 10; i < length; i++ ) {
            this.years.push(currentYear + i);
        }
    }

    // private _getDaysMonthsYears() {
    //     const currentYear = moment().format('YYYY');
    //     for (let i = 0, length = 100; i <= length; i++) {
    //         let daysMonth: any = {};
    //         const year = Number(currentYear) - i;
    //         const daysMonths = this._daysMonthsGenerate(year);
    //         daysMonth['' + year] = {
    //             days: daysMonths.days,
    //             months: daysMonths.months
    //         };
    //         this.years.push('' + year);
    //         this.daysMonths.push(daysMonths);
    //     };
    //
    //     console.log('this.daysMonths', this.daysMonths);
    // }

    private _isEditable() {
        this.activatedRoute.queryParams.subscribe(params => {
            if (params && params.hasOwnProperty('edit') && params.edit) {
                this.editable = true;
            }
        });

    }
}
