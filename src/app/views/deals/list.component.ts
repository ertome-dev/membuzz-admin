import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

import {
    CouponService
} from '../../services';

@Component({
    template: `
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Filter by</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                id="name"
                                                placeholder="Deal name"
                                                [(ngModel)]="deal.name"
                                        >
                                    </div>
                                </div>
                                <!--<div class="col-sm-6">-->
                                    <!--<div class="form-group">-->
                                        <!--<label for="email">Email</label>-->
                                        <!--<input-->
                                                <!--type="email"-->
                                                <!--class="form-control"-->
                                                <!--id="email"-->
                                                <!--placeholder="Enter deal email"-->
                                                <!--[(ngModel)]="deal.email"-->
                                        <!--&gt;-->
                                    <!--</div>-->
                                <!--</div>-->
                            </div>
                            <!--<div class="row">-->
                                <!--<div class="col-sm-6">-->
                                    <!--<div class="form-group">-->
                                        <!--<label for="type">Type</label>-->
                                        <!--<select class="form-control" id="type" [(ngModel)]="deal.type">-->
                                            <!--<option *ngIf="deal.type !== ''" value="{{''}}">-->
                                                <!--Stop filter by type-->
                                            <!--</option>-->
                                            <!--<option value="1">1</option>-->
                                            <!--<option value="2">2</option>-->
                                        <!--</select>-->
                                    <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-6">-->
                                    <!--<div class="form-group">-->
                                        <!--<label for="type">Status</label>-->
                                        <!--<select class="form-control" id="status" [(ngModel)]="deal.status">-->
                                            <!--<option *ngIf="deal.status !== ''" value="{{''}}">-->
                                                <!--Stop filter by status-->
                                            <!--</option>-->
                                            <!--<option value="1">1</option>-->
                                            <!--<option value="2">2</option>-->
                                        <!--</select>-->
                                    <!--</div>-->
                                <!--</div>-->
                            <!--</div>-->
                            <!--<div class="row">-->
                                <!--<div class="col-sm-6">-->
                                    <!--<div class="form-group">-->
                                        <!--<label for="day">Age</label>-->
                                        <!--<select class="form-control" id="day" [(ngModel)]="deal.ageLimit">-->
                                            <!--<option-->
                                                    <!--*ngIf="deal.ageLimit !== ''"-->
                                                    <!--value="{{''}}"-->
                                            <!--&gt;Stop filter by age</option>-->
                                            <!--<option-->
                                                    <!--value="0"-->
                                            <!--&gt;0+</option>-->
                                            <!--<option-->
                                                    <!--value="6"-->
                                            <!--&gt;6+</option>-->
                                            <!--<option-->
                                                    <!--value="16"-->
                                            <!--&gt;16+</option>-->
                                            <!--<option-->
                                                    <!--value="18"-->
                                            <!--&gt;18+</option>-->
                                            <!--<option-->
                                                    <!--value="21"-->
                                            <!--&gt;21+</option>-->
                                        <!--</select>-->
                                    <!--</div>-->
                                <!--</div>-->
                            <!--</div>&lt;!&ndash;/.row&ndash;&gt;-->
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col--><!--/.col-->
        
            <div class="row">
                <div *ngFor="let deal of filterDealList(dealList); let index = index;" class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                    <app-deal-card [deal]="deal" (removeDeal)="this.removeDeal(index)"></app-deal-card>
                </div>
            </div>
            
          
                <p class="text-center text-justify">
                    <span *ngIf="dealList.length === 0">
                        No deals.
                    </span>
                    <span *ngIf="dealList.length !== 0 && filterDealList(dealList).length === 0">
                        No deals with these filter params.
                    </span>
                </p>
            
        </div>
    `,
})

export class DealsListComponent implements OnInit {
    public deal = {
        name: '',
    };

    public dealList = [
        {
            "id":"1",
            "name": "deal example",
            "full_price": 100,
            "deal_price": 75,
            "discount": .25,
            "deal_redemption": 1, // Once a day per user / One time only per user
            "deal_content": "deal content",
            "deal_photo": "photo path",
            "deal_text": "deal text",
            "deal_items": 1,
            "deal_purchase": 2,
            "deal_start_date": '02-03-2018',
            "deal_end_date": '03-03-2018'
        },
    ];

    constructor(
        private couponService: CouponService
    ) {}

    ngOnInit() {
        this.getCouponList();
    }

    public filterDealList(dealList) {
        return dealList.filter(deal => {
            const matchByFilterResult =
                (this.deal.name ? (deal.name.indexOf(this.deal.name) === 0) : true);
           return matchByFilterResult;
        });
    }

    removeDeal(index) {
        this.dealList.splice(index, 1);
    }

    getCouponList() {
        this.couponService.getCouponList().then(response => {
            console.log('couponList response', response);
        });
    }


}