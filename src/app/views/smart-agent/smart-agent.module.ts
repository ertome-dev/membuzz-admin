import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SmartAgentComponent } from './smart-agent.component';

import { SmartAgentRoutingModule } from './smart-agent-routing.module';



@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        RouterModule,
        SmartAgentRoutingModule,
    ],
    declarations: [
        SmartAgentComponent,
    ]
})
export class SmartAgentModule { }
