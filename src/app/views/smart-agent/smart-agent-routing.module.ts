import { NgModule } from '@angular/core';
import {
    Routes,
    RouterModule
} from '@angular/router';

import { SmartAgentComponent } from './smart-agent.component';


const routes: Routes = [
    {
        path: '',
        component: SmartAgentComponent,
        data: {
            title: 'Smart Agent'
        }
        // children: [
        //     {
        //         path: '',
        //         component: BusinessListComponent,
        //         data: {
        //             title: 'Business'
        //         }
        //     },
        //     {
        //         path: ':id',
        //         component: BusinessProfileComponent,
        //         data: {
        //             title: 'Business'
        //         }
        //     },
        //     {
        //         path: ':id/:edit',
        //         component: BusinessProfileComponent,
        //         data: {
        //             title: 'Edit Business'
        //         }
        //     },
        //     {
        //         path: '/create',
        //         component: BusinessProfileComponent,
        //         data: {
        //             title: 'New Business'
        //         }
        //     }
        // ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SmartAgentRoutingModule {}