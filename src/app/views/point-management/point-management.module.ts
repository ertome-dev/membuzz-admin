import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { PointManagementComponent } from './point-management.component';

import { PointManagementRoutingModule } from './point-management-routing.module';



@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        RouterModule,
        PointManagementRoutingModule,
    ],
    declarations: [
        PointManagementComponent,
    ]
})
export class PointManagementModule { }
