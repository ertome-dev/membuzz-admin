import { Component } from '@angular/core';
import { Router } from '@angular/router';


@Component({
    templateUrl: 'point-management.component.html'
})
export class PointManagementComponent {
    constructor(
        private router: Router
    ) {
        console.log('PointManagementComponent constructor');
    }

    public goToMain() {
        this.router.navigateByUrl('/users');
    }
}