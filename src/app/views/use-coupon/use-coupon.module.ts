import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { UseCouponComponent } from './use-coupon.component';

import { UseCouponRoutingModule } from './use-coupon-routing.module';



@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        RouterModule,
        UseCouponRoutingModule,
    ],
    declarations: [
        UseCouponComponent,
    ]
})
export class UseCouponModule { }
