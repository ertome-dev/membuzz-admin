import { NgModule } from '@angular/core';
import {
    Routes,
    RouterModule
} from '@angular/router';

import { UseCouponComponent } from './use-coupon.component';


const routes: Routes = [
    {
        path: '',
        component: UseCouponComponent,
        data: {
            title: 'Use of Coupon'
        }
        // children: [
        //     {
        //         path: '',
        //         component: BusinessListComponent,
        //         data: {
        //             title: 'Business'
        //         }
        //     },
        //     {
        //         path: ':id',
        //         component: BusinessProfileComponent,
        //         data: {
        //             title: 'Business'
        //         }
        //     },
        //     {
        //         path: ':id/:edit',
        //         component: BusinessProfileComponent,
        //         data: {
        //             title: 'Edit Business'
        //         }
        //     },
        //     {
        //         path: '/create',
        //         component: BusinessProfileComponent,
        //         data: {
        //             title: 'New Business'
        //         }
        //     }
        // ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UseCouponRoutingModule {}