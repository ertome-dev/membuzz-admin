import { Component, OnInit } from '@angular/core';
import { EmailValidator } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

// Services
import { UserService } from '../../services/user.service';

@Component({
    templateUrl: 'user-profile.component.html'
})
export class UserProfileComponent implements OnInit {
    public months = [];
    public years = [];
    public days = [];
    public user: any = null;

    public userTypes: any[] = [];
    public userStatuses: any[] = [];
    public editable: boolean = false;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private userService: UserService
    ) {}

    ngOnInit() {
        this._pagePrepare();

        this._getUserTypes();
        this._getUserStatuses();
        this._yearGenerate();
        this._monthsGenerate();
    }

    public cancel() {
        this.router.navigate(['users']);
    }

    public submit(user) {
        // const birth = moment(`${user.birthYear} ${user.birthMonth} ${user.birthDay}`).format('YYYY-MM-DD');
        // {"id":"3", "us_email":"qq@qq.qq", "us_pass":"111111", "us_name":"bruce wayne", "status":"1", "type":"2", "birth_date":"1980-01-01"}
        // console.log('birth', birth);


        const parsedUser = this._userParse(user);
        console.log('parsedUser', parsedUser);
        this._updateUser(parsedUser);

    }

    public daysGenerate(month, year) {
        this.days = [];
        for (let j = 1, daysLength = moment(`${year}-${month}`, 'YYYY-MM').daysInMonth(); j <= daysLength; j++) {
            this.days.push(j);
        };
    }

    ////

    private _getUser(id: string) {
        if (id === 'create') {
            this.editable = true;
            this.user = {
                type: 'client',
                status: 'blocked'
            };
            this.user.birth_date = moment().add('year', -18).format('YYYY-MM-DD');
            this._userDateParse(this.user.birth_date, this.daysGenerate.bind(this));
        } else {
            // async get user
            console.warn('ID', id);
            this.userService
                .getUser(id)
                .then(response => {
                    console.log('response ==> ', response);
                    if (response.success === 1) {
                        this.user = response.user;
                        this._userDateParse(this.user.birth_date, this.daysGenerate.bind(this));
                    }
                });
        }
    }

    private _userDateParse(usrDate, afterDateParsingCB) {
        const userBirthDay = moment(usrDate);
        this.user.birthDay = userBirthDay.format('D');
        this.user.birthMonth = userBirthDay.format('M');
        this.user.birthYear = userBirthDay.format('YYYY');

        afterDateParsingCB(this.user.birthYear);

    }

    private _monthsGenerate() {
        this.months = [];
        for (let i = 1, monthsLength = 12; i <= monthsLength; i++) {
            this.months.push(i);
        };
    }

    private _yearGenerate() {
        this.years = [];
        const currentYear = Number(moment().format('YYYY'));
        for (let i = 1, length = 100; i < length; i++ ) {
            this.years.push(currentYear - i);
        }
    }

    private _pagePrepare() {
        this.activatedRoute.params.subscribe(params => {
            this._getUser(params.id);
        });
        this._isEditable();
    }

    private _getUserTypes(): void {
        this.userService
            .getUserTypes()
            .then(response => {
                console.log('user types response ==> ', response);
                if (response.success) {
                    this.userTypes = response.types;
                }
            });
    }

    private _getUserStatuses(): void {
        this.userService
            .getUserStatuses()
            .then(response => {
                if (response.success) {
                    this.userStatuses = response.statuses;
                }
            });
    }

    private _isEditable() {
        this.activatedRoute.queryParams.subscribe(params => {
            if (params && params.hasOwnProperty('edit') && params.edit) {
                this.editable = true;
            }
        });

    }

    private _createUser(user): void {
        console.log('user ==> ', user);
    }

    private _updateUser(user): void {
        this.userService
            .updateUser(user)
            .then(response => {
                console.log('response ===> ', response);
                // todo: redirect to list
            });
    }

    private _userParse(user) {// this.user
        const birth = moment(`${user.birthYear} ${user.birthMonth} ${user.birthDay}`).format('YYYY-MM-DD');

        return Object.assign({}, {
            id: user.id,
            us_name: user.us_name,
            us_image: user.us_image,
            us_email: user.us_email,
            us_pass: user.us_pass,
            status: this._userStatusesParse(user.status),
            type: this._userTypesParse(user.type),
            birth_date: birth
        });

        // {"us_email":"neww@qq.qq", "us_pass":"111111", "us_name":"alfred", "us_image":"jseof.jpg", "status":"1", "type":"3"}
    }

    private _userTypesParse(type: string): string {
        let obj = {};
        this.userTypes.forEach(item => {
            obj[item.type] = item;
        });

        return '' + obj[type].id;
    }

    private _userStatusesParse(status: string): string {
        let obj = {};
        this.userStatuses.forEach(item => {
            obj[item.status] = item;
        });

        return '' + obj[status].id;
    }
}
