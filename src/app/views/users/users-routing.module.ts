import { NgModule } from '@angular/core';
import {
    Routes,
    RouterModule
} from '@angular/router';

import { UsersComponent } from './users.component';
import { UsersListComponent } from './list.component';
import { UserProfileComponent } from './user-profile.component';
// import { CreateUserComponent } from './create.component';

const routes: Routes = [
    {
        path: '',
        component: UsersComponent,
        children: [
            {
              path: '',
                component: UsersListComponent,
                data: {
                  title: 'Users'
                }
            },
            {
                path: ':id',
                component: UserProfileComponent,
                data: {
                    title: 'User'
                }
            },
            {
                path: ':id/:edit',
                component: UserProfileComponent,
                data: {
                    title: 'Edit User'
                }
            },
            {
                path: '/create',
                component: UserProfileComponent,
                data: {
                    title: 'New User'
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersRoutingModule {}
