import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { UsersComponent } from './users.component';
import { UserProfileComponent } from './user-profile.component';
import { UsersListComponent } from './list.component';

import { UserCardComponent } from '../../components/user-card/user-card.component';

import { UsersRoutingModule } from './users-routing.module';



@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    RouterModule,
    UsersRoutingModule,
    BsDropdownModule,
    ButtonsModule.forRoot()
  ],
  declarations: [
      UsersComponent,
      UserProfileComponent,
      UsersListComponent,
      UserCardComponent
  ]
})
export class UsersModule { }
