import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

// Services
import { UserService } from '../../services';

@Component({
    template: `
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Filter by</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                id="name"
                                                placeholder="User name"
                                                [(ngModel)]="user.name"
                                        >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input
                                                type="email"
                                                class="form-control"
                                                id="email"
                                                placeholder="Enter user email"
                                                [(ngModel)]="user.email"
                                        >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="type">Type</label>
                                        <select class="form-control" id="type" [(ngModel)]="user.type">
                                            <option *ngIf="user.type !== ''" value="{{''}}">
                                                Stop filter by type
                                            </option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="type">Status</label>
                                        <select class="form-control" id="status" [(ngModel)]="user.status">
                                            <option *ngIf="user.status !== ''" value="{{''}}">
                                                Stop filter by status
                                            </option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="day">Age</label>
                                        <select class="form-control" id="day" [(ngModel)]="user.ageLimit">
                                            <option
                                                    *ngIf="user.ageLimit !== ''"
                                                    value="{{''}}"
                                            >Stop filter by age</option>
                                            <option
                                                    value="0"
                                            >0+</option>
                                            <option
                                                    value="6"
                                            >6+</option>
                                            <option
                                                    value="16"
                                            >16+</option>
                                            <option
                                                    value="18"
                                            >18+</option>
                                            <option
                                                    value="21"
                                            >21+</option>
                                        </select>
                                    </div>
                                </div>
                            </div><!--/.row-->
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col--><!--/.col-->
        
            <div class="row">
                <div *ngFor="let user of filterUserList(userList); let index = index;" class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                    <app-user-card [user]="user" (removeUser)="this.removeUser(index)"></app-user-card>
                </div>
            </div>
            <p class="text-center text-justify">
                    <span *ngIf="userList.length === 0">
                        No users.
                    </span>
                    <span *ngIf="userList.length !== 0 && filterUserList(userList).length === 0">
                        No users with these filter params.
                    </span>
                </p>
        </div>
    `,
})

export class UsersListComponent implements OnInit {
    public userList: any[] = [];
    public userTypes: any[] = [];
    public userStatuses: any[] = [];
    public userGenders: any[] = [];

    public user: any = {
        name: '',
        email: '',
        type: '',
        status: '',
        ageLimit: ''
    };

    constructor (
        private userService: UserService
    ) {

    }

    ngOnInit() {
        this._getUserList();
        this._getUserTypes();
        this._getUserStatuses();
        this._getUserGenders();
    }

    // public userList = [
    //     {"id":"1", "us_email":"q1@qq.qq", "us_pass":"111111", "us_name":"batman", "status":"1", "type":"2", "birth_date":"2014-01-01"},
    //     {"id":"2", "us_email":"q2@qq.qq", "us_pass":"111111", "us_name":"superman", "status":"1", "type":"1", "birth_date":"2018-01-01"},
    //     {"id":"3", "us_email":"qsdfsdfsdfsdfsd3@qq.qq", "us_pass":"111111", "us_name":"spiderman", "status":"1", "type":"2", "birth_date":"1991-01-01"},
    //     {"id":"4", "us_email":"q4@qq.qq", "us_pass":"111111", "us_name":"iron man", "status":"2", "type":"1", "birth_date":"2000-01-01"},
    // ];

    public filterUserList(userList) {
        return userList.filter(user => {
            const matchByFilterResult =
                (this.user.name ? (user.us_name.indexOf(this.user.name) === 0) : true) &&
                (this.user.email ? (user.us_email.indexOf(this.user.email) === 0) : true) &&
                (this.user.type ? ('' + user.type === '' + this.user.type) : true) &&
                (this.user.status ? ('' + user.status === '' + this.user.status) : true) &&
                (this.user.ageLimit ? ((Math.abs(moment(user.birth_date).diff(moment(), 'years')) || 0) >= Number(this.user.ageLimit)) : true);
            return matchByFilterResult;
        });
    }

    public removeUser(index) {
        this.userList.splice(index, 1);
    }

    ///

    private _getUserList(): void {
        this.userService
            .getUserList()
            .then(response => {
                if (response.success) {
                    console.log('response.users', response.users);
                    this.userList = response.users;
                }
            }, (errors) => {
                console.log('errors --> ', errors);
            });
    }

    private _getUserTypes(): void {
        this.userService
            .getUserTypes()
            .then(response => {
                console.log('user types response ==> ', response);
                if (response.success) {
                    this.userTypes = response.types;
                }
            });
    }

    private _getUserStatuses(): void {
        this.userService
            .getUserStatuses()
            .then(response => {
                console.log('user statuses response ==> ', response);
                if (response.success) {
                    this.userStatuses = response.statuses;
                }
            });
    }

    private _getUserGenders(): void {
        this.userService
            .getUserGenders()
            .then(response => {
                console.log('user genders response ==> ', response);
                if (response.success) {
                    this.userGenders = response.genders;
                }
            });
    }
}