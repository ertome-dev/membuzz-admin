import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Auth Guards
import {
    IsLoggedIn,
    IsLoggedOut
} from './auth-guards';

// Import Containers
import {
    FullLayoutComponent,
    SimpleLayoutComponent
} from './containers';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'users',
        pathMatch: 'full',
    },
    {
        path: '',
        component: FullLayoutComponent,
        children: [
            {
                path: 'users',
                loadChildren: './views/users/users.module#UsersModule',
                canActivate: [IsLoggedIn]
            },
            {
                path: 'business',
                loadChildren: './views/business/business.module#BusinessModule',
                canActivate: [IsLoggedIn]
            },
            {
                path: 'deals',
                loadChildren: './views/deals/deals.module#DealsModule',
                canActivate: [IsLoggedIn]
            },
            //

            {
                path: 'point-management',
                loadChildren: './views/point-management/point-management.module#PointManagementModule',
                canActivate: [IsLoggedIn]
            },
            {
                path: 'smart-agent',
                loadChildren: './views/smart-agent/smart-agent.module#SmartAgentModule',
                canActivate: [IsLoggedIn]
            },
            {
                path: 'use-coupon',
                loadChildren: './views/use-coupon/use-coupon.module#UseCouponModule',
                canActivate: [IsLoggedIn]
            },
            {
                path: 'contact-us',
                loadChildren: './views/contact-us/contact-us.module#ContactUsModule',
                canActivate: [IsLoggedIn]
            },
        ]
    },
    {
        path: '',
        component: SimpleLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './views/pages/pages.module#PagesModule',
                canActivate: [IsLoggedOut]
            }
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
